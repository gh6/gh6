var solr = require('solr');

var client = solr.createClient();

var client = solr.createClient({
  host:'52.45.164.223',
  port:80,
  core:'gh6',
  path:'/solr',
  secure:false
});
client.autoCommit = true;
client.basicAuth('nginx','gh6');

var newRec = {
  "Program_Name":["MYTEST2"],
  "Type":["VA"],
  "Address":["East St Louis, IL 62205"],
  "Type_of_Program":["Emergency"],
  "Contact_Info":["Sarah McCabe"],
  "Service_Capacity":["5 beds (working towards 10)"],
  "Service_Capacity_Total":[5],
  "Serves_Vets_Only":["YES"],
  "Serves_HIV_Only":["NO"],
  "Age_Min":[18],
  "Single_Men":["NO"],
  "Single_Women":["YES"],
  "Do_They_Serve_Families":["YES, Women can have children under 18yrs."],
  "Must_enter_sober":["NO, screenings but sober while there"],
  "Must_have_ID":["NO"],
  "Must_have_BirthCertificate":["NO"],
  "Must_have_Social_Security_Card":["NO"],
  "Must_have_aviable_income":["NO"],
  "Accepts_Sex_Offenders":["NO"],
  "Requires_a_Referral_from_a_specific_entity":["YES, HOPE Recovery Center"],
  "Residency_Requirements":["NO"],
  "Located_in_which_county":["st. clair county"],
  "Is_This_Program_Congregate_Style":["might have own room; depends on if single vs. family"],
  "Sensitive_To_Transgender_Population":["YES"],
  "Program_Length":["90days"],
  "_Agency_Service_Hours_":["Mon-Fri 8a-5p HOPE, after hours through John Cochran Hospital"],
  "_Program_Service_Hours_":["24hr service"]};


client.add(newRec,function(err,obj){
  if(err){
    console.log(err);
  }else{
    console.log('Solr response:', obj);
  }
});