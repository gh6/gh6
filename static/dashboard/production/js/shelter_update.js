function getParameterByName(name, url) {
    if (!url) {
      url = window.location.href;
    }
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

var id = getParameterByName("id");
$("#demo-form2").submit(function(event) {
  event.preventDefault();
  window.location.href="tables_dynamic.html";
});

if( id === null ) {
  $("#shelter_small_txt").text("Create Shelter");
  var formElem = '<label id="shelter_type_label" for="shelter_type">Select a Shelter Type to get Started *:</label>';
  formElem += '<select id="shelter_type" class="form-control" required onchange="populateFields(this)">';
  formElem += '<option value="">Add a new Shelter...</option>';
    formElem += '<option value="VA">VA</option>';
    formElem += '<option value="YouthPrograms">YouthPrograms</option>';
    formElem += '<option value="Independent">Independent</option>';
    formElem += '</select>';
  $("#demo-form2").append(formElem);
} else {
  $("#shelter_small_txt").text("Modify Shelter");
  var settings = {
    "async": true,
    "crossDomain": true,
    "url": "/solr2/gh6/get?id=" + id,
    "method": "GET"
  };

  $.ajax(settings).done(function (response) {
    var shelter = JSON.parse(response);
    for (var pName in shelter.doc) {
      var formElem = "";
      var to = typeof shelter.doc[pName][0];
      if ((to === "string") && (shelter.doc[pName][0].startsWith("YES") || shelter.doc[pName][0].startsWith("NO"))) {
        var checkedText = (shelter.doc[pName][0].startsWith("YES")) ? "checked" : "";
        formElem += '<div class="form-group"><label class="control-label col-md-3 col-sm-3 col-xs-12">';
        formElem += pName.replace(/_/g, ' ')
        formElem += '<span class="required">*</span></label><div class="col-md-6 col-sm-6 col-xs-12">';
        formElem += '<input type="checkbox" class="js-switch" ' + checkedText + ' />';
        var splitStr = shelter.doc[pName][0].split(",");
        if (shelter.doc[pName][0].length > 3) {
          formElem += " " + shelter.doc[pName][0];
        }
      } else {
        var value = "";
        if (Array.isArray(shelter.doc[pName])) {
          value = shelter.doc[pName][0];
        } else {
          value = shelter.doc[pName];
        }
        formElem = '<div class="form-group"><label class="control-label col-md-3 col-sm-3 col-xs-12">';
        formElem += pName.replace(/_/g, ' ');
        formElem += '<span class="required">*</span></label><div class="col-md-6 col-sm-6 col-xs-12">';
        formElem += '<input type="text" id="' + pName + '" required="required" class="form-control col-md-7 col-xs-12" value="' + value + '">';
        formElem += '</div></div>';
      }
      
      $("#demo-form2").append(formElem);
    }

    var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));

    elems.forEach(function (html) {
      var switchery = new Switchery(html);
    });

  });
}

function populateFields(evt) {
  
  $("#shelter_type").remove();
  $("#shelter_type_label").remove();

  var settings = {
    "async": true,
    "crossDomain": true,
    "url": '/solr2/gh6/select?indent=on&q=Type:"' + evt.value + '"&rows=1&start=0&wt=json',
    "method": "GET"
  };
  $.ajax(settings).done(function (response) {
    var shelter = JSON.parse(response);
    for (var pName in shelter.response.docs[0]) {
      var formElem = "";
      var to = typeof shelter.response.docs[0][pName][0];
      if ((to === "string") && (shelter.response.docs[0][pName][0].startsWith("YES") || shelter.response.docs[0][pName][0].startsWith("NO"))) {
        formElem += '<div class="form-group"><label class="control-label col-md-3 col-sm-3 col-xs-12">';
        formElem += pName.replace(/_/g, ' ')
        formElem += '<span class="required">*</span></label><div class="col-md-6 col-sm-6 col-xs-12">';
        formElem += '<input type="checkbox" class="js-switch" />';
        var splitStr = shelter.response.docs[0][pName][0].split(",");
        if (shelter.response.docs[0][pName][0].length > 3) {
          formElem += " " + shelter.response.docs[0][pName][0];
        }
      } else {
        var value = "";
        if (Array.isArray(shelter.response.docs[0][pName])) {
          value = shelter.response.docs[0][pName][0];
        } else {
          value = shelter.response.docs[0][pName];
        }
        formElem = '<div class="form-group"><label class="control-label col-md-3 col-sm-3 col-xs-12">';
        formElem += pName.replace(/_/g, ' ');
        formElem += '<span class="required">*</span></label><div class="col-md-6 col-sm-6 col-xs-12">';
        var value = "";
        if(pName === "Type") value = evt.value;
        formElem += '<input type="text" id="' + pName + '" required="required" class="form-control col-md-7 col-xs-12" value="' + value + '">';
        formElem += '</div></div>';
      }
      
      $("#demo-form2").append(formElem);
    }

    var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));

    elems.forEach(function (html) {
      var switchery = new Switchery(html);
    });

  });
  
}