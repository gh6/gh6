var fs = require('fs');
var parse = require('csv').parse;

var AWS = require("aws-sdk");

AWS.config.loadFromPath('../config.json');

AWS.config.update({
  region: "us-east-1"
});

var docClient = new AWS.DynamoDB.DocumentClient();

var table = "clients";

var dischargeStatus = {
  1: "Honorable",
  2: "General_under_honorable_conditions",
  4: "Bad_conduct",
  5: "Dishonorable",
  6: "Under_other_than_honorable_conditions",
  7: "Uncharacterized",
  8: "Client_does_not_know",
  9: "Client_refused",
  99: "Data_not_collected"
};

var branches = {
  1: "Army",
  2: "AirForce",
  3: "Navy",
  4: "Marines",
  6: "CoastGuard",
  8: "Client_does_not_know",
  9: "Client_refused",
  99: "Data_not_collected"
};

var gender = {
  0: "Female",
  1: "Male",
  2: "Transgender_male_to_female",
  3: "Transgender_female_to_male",
  4: "Other",
  8: "Client_does_not_know",
  9: "Client_refused",
  99: "Data_not_collected"
};

var parser = parse({delimiter: ','}, function(err,data) {

  for(var i=0;i<data.length;i++) {
    var d = data[i];
    // for(var j=0;j<d.length;j++) {
    //   console.log(j + ":" + d[j]);
    // }
    var client = {};
    client.uuid = d[0];
    client.name = {};
    if(d[1] !== '') client.name.first = d[1];
    if(d[2] !== '') client.name.middle = d[2];
    if(d[3] !== '') client.name.last = d[3];
    client.ssn = d[5];
    if(d[7] !== 'NULL') client.dob = new Date(d[7]).toISOString();
    if(d[9]  === "1") client.race = "AmericanIndian_AKNative";
    if(d[10] === "1") client.race = "Asian";
    if(d[11] === "1") client.race = "Black";
    if(d[12] === "1") client.race = "NativeHispanic";
    if(d[13] === "1") client.race = "White";
    if(d[14] === "1") client.race = "None";
    client.gender = gender[d[15]];
    if(d[16] === "1") client.othergender = "yes";
    if(d[17] === "1") {
      client.veteran = {};
      if(d[18] !== 'NULL') client.veteran.entered = d[18];
      if(d[19] !== 'NULL') client.veteran.seperated = d[19];
      client.veteran.wars = [];
      if(d[20] === "1") client.veteran.wars.push("Word_War_II");
      if(d[21] === "1") client.veteran.wars.push("Korean_War");
      if(d[22] === "1") client.veteran.wars.push("Vietnam_War");
      if(d[23] === "1") client.veteran.wars.push("Desert_Storm");
      if(d[24] === "1") client.veteran.wars.push("AfghanistanOEF");
      if(d[25] === "1") client.veteran.wars.push("IraqOIF");
      if(d[26] === "1") client.veteran.wars.push("IraqOND");
      if(d[27] === "1") client.veteran.wars.push("OtherTheater");
      client.veteran.branch = branches[d[28]];
      client.veteran.discharge = dischargeStatus[d[29]];
    }
    client.created = new Date(d[30]).toISOString();
    client.updated = new Date(d[31]).toISOString();
    client.userid = d[32];

    putClientInDynamo(client);
  }


});

fs.createReadStream(__dirname + '/Clients.csv').pipe(parser);


function putClientInDynamo(client) {
  var params = {
    TableName:table,
    Item: client
  };

  console.log("Adding a new client...");
  docClient.put(params, function(err, data) {
    if (err) {
      console.error("Unable to add item. Error JSON:", JSON.stringify(err, null, 2));
      console.log(client);
    } else {
      console.log("Added item:", JSON.stringify(data, null, 2));
    }
  });

}