/**
 * @file
 * Inventory functions
 *
 */

var aws = require('aws-sdk');
var uuid = require('uuid');
var httpStatusCodes = require('http-status-codes');
var logger = require('../utils/logger').logger;

aws.config.update({region: 'us-east-1'});

var dynamodbDoc = new aws.DynamoDB.DocumentClient();

const TableName = "inventory";

function queryInventories(inputs, callback) {
  // This is rushed and messy...
  if (inputs.ShelterName) {
    var params = {
      TableName: TableName,
      Key: {
        ShelterName: inputs.ShelterName
      }
    };

    dynamodbDoc.get(params, function (err, data) {
      if (err) {
        var errorPayload = {
          status: "error",
          error: "Failed Reading Inventory",
          timestamp: new Date().toJSON()
        };
        if (err.message === "The provided key element does not match the schema") {
          errorPayload['code'] = httpStatusCodes.NOT_FOUND;
        }
        else {
          errorPayload['code'] = httpStatusCodes.INTERNAL_SERVER_ERROR;
        }
        logger.error({
          method: "readInventory",
          payload: errorPayload,
          trace: err
        });
        callback(errorPayload);
      }
      else {
        // These return values depend on whether the item was found or not
        var count = data.Item ? 1 : 0;
        var result = data.Item ? data.Item : {};
        var payload = {
          count: count,
          status: "success",
          code: httpStatusCodes.OK,
          message: "Inventory Read",
          UUID: inputs.UUID,
          result: result,
          timestamp: new Date().toJSON()
        };
        logger.info({
          method: "readInventory",
          payload: payload
        });
        callback(null, payload);
      }
    });
  }
  else {
    var params = {
      TableName: TableName
    };

    dynamodbDoc.scan(params, function (err, data) {
      if (err) {
        var errorPayload = {
          status: "error",
          code: httpStatusCodes.INTERNAL_SERVER_ERROR,
          error: "Failed Retrieving Inventories"
        };
        logger.error({
          method: "queryInventories",
          payload: errorPayload,
          trace: err
        });
        callback(err, errorPayload);
      }
      else {
        var payload = {
          count: data.Count,
          status: "success",
          code: httpStatusCodes.OK,
          message: "Inventories Retrieved",
          results: data.Items
        };
        logger.info({
          method: "queryInventories",
          payload: payload
        });
        callback(null, payload);
      }
    });
  }
}

function createInventory (inputs, callback) {
  var curDate = new Date().toJSON();
  var params = {
    TableName: TableName,
    Item: {
      UUID: uuid.v4(),
      InventoryID: inputs.InventoryID,
      ProjectID: inputs.ProjectID,
      CoCCode: inputs.CoCCode,
      InformationDate: inputs.InformationDate,
      HouseholdType: inputs.HouseholdType,
      BedType: inputs.BedType,
      Availability: inputs.Availability,
      UnitInventory: inputs.UnitInventory,
      BedInventory: inputs.BedInventory,
      CHBedInventory: inputs.CHBedInventory,
      VetBedInventory: inputs.VetBedInventory,
      YouthBedInventory: inputs.YouthBedInventory,
      YouthAgeGroup: inputs.YouthAgeGroup,
      InventoryStartDate: inputs.InventoryStartDate,
      InventoryEndDate: inputs.InventoryEndDate,
      HMISParticipatingBeds: inputs.HMISParticipatingBeds,
      DateCreated: curDate,
      DateUpdated: curDate,
      UserID: inputs.UserID,
      DateDeleted: inputs.DateDeleted,
      ExportID: inputs.ExportID,
      ShelterName: inputs.ShelterName
    },
    ExpressionAttributeNames: {"#u": "UUID"},
    ConditionExpression: "attribute_not_exists(#u)"
  };

  dynamodbDoc.put(params, function (err) {
    if (err) {
      var errorPayload = {
        status: "error",
        code: httpStatusCodes.INTERNAL_SERVER_ERROR,
        error: "Failed Creating Inventory",
        timestamp: new Date().toJSON()
      };
      logger.error({
        method: "createInventory",
        payload: errorPayload,
        trace: err
      });
      callback(err, errorPayload);
    }
    else {
      var payload = {
        count: 1,
        status: "success",
        code: httpStatusCodes.OK,
        message: "Inventory Created",
        results: params.Item,
        timestamp: new Date().toJSON()
      };
      logger.info({
        method: "createInventory",
        payload: payload
      });
      callback(null, payload);
    }
  });
}

function readInventory (inputs, callback) {
  var params = {
    TableName: TableName,
    Key: {
      UUID: inputs.UUID
    }
  };

  dynamodbDoc.get(params, function (err, data) {
    if (err) {
      var errorPayload = {
        status: "error",
        code: httpStatusCodes.INTERNAL_SERVER_ERROR,
        error: "Failed Reading Inventory",
        timestamp: new Date().toJSON()
      };
      logger.error({
        method: "readInventory",
        payload: errorPayload,
        trace: err
      });
      callback(err, errorPayload);
    }
    else {
      // These return values depend on whether the item was found or not
      var count = data.Item ? 1 : 0;
      var result = data.Item ? data.Item : {};
      var payload = {
        count: count,
        status: "success",
        code: httpStatusCodes.OK,
        message: "Inventory Read",
        UUID: inputs.UUID,
        result: result,
        timestamp: new Date().toJSON()
      };
      logger.info({
        method: "readInventory",
        payload: payload
      });
      callback(null, payload);
    }
  });
}

function updateInventory (inputs, callback) {
  var curDate = new Date().toJSON();
  var updateExpression = "SET " +
    "InventoryID=:InventoryID" +
    ",ProjectID=:ProjectID" +
    ",CoCCode=:CoCCode" +
    ",InformationDate=:InformationDate" +
    ",HouseholdType=:HouseholdType" +
    ",BedType=:BedType" +
    ",Availability=:Availability" +
    ",UnitInventory=:UnitInventory" +
    ",BedInventory=:BedInventory" +
    ",CHBedInventory=:CHBedInventory" +
    ",VetBedInventory=:VetBedInventory" +
    ",YouthBedInventory=:YouthBedInventory" +
    ",YouthAgeGroup=:YouthAgeGroup" +
    ",InventoryStartDate=:InventoryStartDate" +
    ",InventoryEndDate=:InventoryEndDate" +
    ",HMISParticipatingBeds=:HMISParticipatingBeds" +
    // ",DateCreated=:DateCreated" +
    ",DateUpdated=:DateUpdated" +
    ",UserID=:UserID" +
    ",DateDeleted=:DateDeleted" +
    ",ExportID=:ExportID" +
    ",ShelterName=:ShelterName";

  var expressionAttributeValues = {
    ":InventoryID": inputs.InventoryID,
    ":ProjectID": inputs.ProjectID,
    ":CoCCode": inputs.CoCCode,
    ":InformationDate": inputs.InformationDate,
    ":HouseholdType": inputs.HouseholdType,
    ":BedType": inputs.BedType,
    ":Availability": inputs.Availability,
    ":UnitInventory": inputs.UnitInventory,
    ":BedInventory": inputs.BedInventory,
    ":CHBedInventory": inputs.CHBedInventory,
    ":VetBedInventory": inputs.VetBedInventory,
    ":YouthBedInventory": inputs.YouthBedInventory,
    ":YouthAgeGroup": inputs.YouthAgeGroup,
    ":InventoryStartDate": inputs.InventoryStartDate,
    ":InventoryEndDate": inputs.InventoryEndDate,
    ":HMISParticipatingBeds": inputs.HMISParticipatingBeds,
    // ":DateCreated": inputs.DateCreated,
    ":DateUpdated": curDate,
    ":UserID": inputs.UserID,
    ":DateDeleted": inputs.DateDeleted,
    ":ExportID": inputs.ExportID,
    ":ShelterName": inputs.ShelterName
  };

  var params = {
    TableName: TableName,
    Key: {
      UUID: inputs.UUID
    },
    UpdateExpression: updateExpression,
    ExpressionAttributeValues: expressionAttributeValues,
    ReturnValues: "ALL_NEW",
    // This is causes an error to be issued if an update is attempted on a UUID that doesn't exist
    //ConditionExpression: "attribute_exists(UUID)"
  };

  dynamodbDoc.update(params, function (err, data) {
    if (err) {
      var errorPayload = {
        status: "error",
        code: httpStatusCodes.INTERNAL_SERVER_ERROR,
        error: "Failed Updating Inventory",
        timestamp: new Date().toJSON()
      };
      logger.error({
        method: "updateInventory",
        payload: errorPayload,
        trace: err
      });
      callback(err, errorPayload);
    }
    else {
      var payload = {
        count: 1,
        status: "success",
        code: httpStatusCodes.OK,
        message: "Inventory Updated",
        results: [data.Attributes],
        timestamp: new Date().toJSON()
      };
      logger.info({
        method: "updateInventory",
        payload: payload
      });
      callback(null, payload);
    }
  });
}

function deleteInventory (inputs, callback) {
  var params = {
    TableName: TableName,
    Key: {
      UUID: inputs.UUID
    },
    // This is causes an error to be issued if a deletion is attempted on a UUID that doesn't exist
    ExpressionAttributeNames: {"#u": "UUID"},
    ConditionExpression: "attribute_exists(#u)"
  };

  dynamodbDoc.delete(params, function (err) {
    if (err) {
      var errorPayload = {
        status: "error",
        code: httpStatusCodes.INTERNAL_SERVER_ERROR,
        error: "Failed Deleting Inventory",
        timestamp: new Date().toJSON()
      };
      logger.error({
        method: "deleteInventory",
        payload: errorPayload,
        trace: err
      });
      callback(err, errorPayload);
    }
    else {
      var payload = {
        status: "success",
        code: httpStatusCodes.OK,
        message: "Inventory Deleted",
        UUID: inputs.UUID,
        timestamp: new Date().toJSON()
      };
      logger.info({
        method: "deleteInventory",
        payload: payload
      });
      callback(null, payload);
    }
  });
}

module.exports = {
  queryInventories: function (req, res) {
    var inputs = {
      ShelterName: req.swagger.params.ShelterName.value
    };
    queryInventories (inputs, function (err, data) {
      if (err) {
        return res.status(err.code).json(err);
      }
      return res.status(data.code).json(data);
    });
  },
  createInventory: function (req, res) {
    var inputs = {
      InventoryID: req.swagger.params.body.originalValue.InventoryID,
      ProjectID: req.swagger.params.body.originalValue.ProjectID,
      CoCCode: req.swagger.params.body.originalValue.CoCCode,
      InformationDate: req.swagger.params.body.originalValue.InformationDate,
      HouseholdType: req.swagger.params.body.originalValue.HouseholdType,
      BedType: req.swagger.params.body.originalValue.BedType,
      Availability: req.swagger.params.body.originalValue.Availability,
      UnitInventory: req.swagger.params.body.originalValue.UnitInventory,
      BedInventory: req.swagger.params.body.originalValue.BedInventory,
      CHBedInventory: req.swagger.params.body.originalValue.CHBedInventory,
      VetBedInventory: req.swagger.params.body.originalValue.VetBedInventory,
      YouthBedInventory: req.swagger.params.body.originalValue.YouthBedInventory,
      YouthAgeGroup: req.swagger.params.body.originalValue.YouthAgeGroup,
      InventoryStartDate: req.swagger.params.body.originalValue.InventoryStartDate,
      InventoryEndDate: req.swagger.params.body.originalValue.InventoryEndDate,
      HMISParticipatingBeds: req.swagger.params.body.originalValue.HMISParticipatingBeds,
      // DateCreated: req.swagger.params.body.originalValue.DateCreated,
      // DateUpdated: req.swagger.params.body.originalValue.DateUpdated,
      UserID: req.swagger.params.body.originalValue.UserID,
      DateDeleted: req.swagger.params.body.originalValue.DateDeleted,
      ExportID: req.swagger.params.body.originalValue.ExportID,
      ShelterName: req.swagger.params.body.originalValue.ShelterName
    };
    createInventory(inputs, function (err, data) {
      if (err) {
        return res.status(err.code).json(err);
      }
      return res.status(data.code).json(data);
    });
  },

  readInventory: function (req, res) {
    var inputs = {
      UUID: req.swagger.params.UUID.value
    };
    readInventory(inputs, function (err, data) {
      if (err) {
        return req.status(err.code).json(err);
      }
      return res.status(data.code).json(data);
    });
  },

  updateInventory: function (req, res) {
    var inputs = {
      UUID: req.swagger.params.UUID.value,
      InventoryID: req.swagger.params.body.originalValue.InventoryID,
      ProjectID: req.swagger.params.body.originalValue.ProjectID,
      CoCCode: req.swagger.params.body.originalValue.CoCCode,
      InformationDate: req.swagger.params.body.originalValue.InformationDate,
      HouseholdType: req.swagger.params.body.originalValue.HouseholdType,
      BedType: req.swagger.params.body.originalValue.BedType,
      Availability: req.swagger.params.body.originalValue.Availability,
      UnitInventory: req.swagger.params.body.originalValue.UnitInventory,
      BedInventory: req.swagger.params.body.originalValue.BedInventory,
      CHBedInventory: req.swagger.params.body.originalValue.CHBedInventory,
      VetBedInventory: req.swagger.params.body.originalValue.VetBedInventory,
      YouthBedInventory: req.swagger.params.body.originalValue.YouthBedInventory,
      YouthAgeGroup: req.swagger.params.body.originalValue.YouthAgeGroup,
      InventoryStartDate: req.swagger.params.body.originalValue.InventoryStartDate,
      InventoryEndDate: req.swagger.params.body.originalValue.InventoryEndDate,
      HMISParticipatingBeds: req.swagger.params.body.originalValue.HMISParticipatingBeds,
      // DateCreated: req.swagger.params.body.originalValue.DateCreated,
      // DateUpdated: req.swagger.params.body.originalValue.DateUpdated,
      UserID: req.swagger.params.body.originalValue.UserID,
      DateDeleted: req.swagger.params.body.originalValue.DateDeleted,
      ExportID: req.swagger.params.body.originalValue.ExportID,
      ShelterName: req.swagger.params.body.originalValue.ShelterName
    };
    updateInventory(inputs, function (err, data) {
      if (err) {
        return req.status(err.code).json(err);
      }
      return res.status(data.code).json(data);
    });
  },

  deleteInventory: function (req, res) {
    var inputs = {
      UUID: req.swagger.params.UUID.value
    };
    deleteInventory(inputs, function (err, data) {
      if (err) {
        return req.status(err.code).json(err);
      }
      return res.status(data.code).json(data);
    });
  }
};
