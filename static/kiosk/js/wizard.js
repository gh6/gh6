$(document).ready(function() {
  startKiosk();
})

function startKiosk(){

  $.typeahead({
    input: ".js-typeahead-lastname",
    order: "asc",
    minLength: 1,
    maxItem: 20,
    dynamic: true,
    offset: true,
    debug: true,
    hint: true,
    display: ["name.last", "name.first"],
    template: '<span id={{uuid}}>{{name.last}}, {{name.first}}</span>',
    source: {
      lastName: {
        ajax: function(query) {
          query = '{"LastName": "' + query + '"}';
          return {
            type: "POST",
            url: "/api/v1/clients/search",
            path: "results",
            dataType: "json",
            contentType: "application/json",
            data: query
          }
        }
      }
    },
    callback: {
      onClickAfter: function (node, a, item, event) {
        $('#firstName').val(item.name.first);
        $('#uuid').val(item.uuid);
        $('.autoFilledData').show()
        event.preventDefault;
      }
    }
  });



  $.typeahead({
    input: ".js-typeahead-ssn",
    order: "asc",
    minLength: 4,
    maxItem: 20,
    dynamic: true,
    offset: true,
    debug: true,
    hint: true,
    source: {
      ssn: {
        ajax: function(query) {
          query = '{"ssn": "' + query + '", "uuid": "'+ $("#uuid").val() +'"}';
          return {
            type: "POST",
            url: "/api/v1/clients/verifySSN",
            path: "result",
            dataType: "json",
            contentType: "application/json",
            data: query
          }
        }
      }
    },
    callback: {
      onPopulateSource: function (node, a, item, event) {
        event.preventDefault;
        if (a.length === 1) {
          $('input#ssn').attr("disabled", true);
          $('input#lastName').attr("disabled", true);
          $('#formAlert').removeClass("alert-info").removeClass("alert-danger").addClass("alert-success").html("Thank you. You've successfully been verified.");
        } else {
          $('#formAlert').removeClass("alert-info").addClass("alert-danger").html("Sorry. You've entered an incorrect SSN. Please try again.");
        }
      }
    }
  });



}