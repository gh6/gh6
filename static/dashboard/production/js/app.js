function getParameterByName(name, url) {
  if (!url) {
    url = window.location.href;
  }
  name = name.replace(/[\[\]]/g, "\\$&");
  var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
    results = regex.exec(url);
  if (!results) return null;
  if (!results[2]) return '';
  return decodeURIComponent(results[2].replace(/\+/g, " "));
}

var shelter = getParameterByName("shelter");
$("#demo-form2").submit(function(event) {
  event.preventDefault();
  window.location.href="tables_dynamic.html";
});


var clientQuerySettings = {
  "async": true,
  "crossDomain": true,
  "url": "/api/v1/clients",
  "method": "GET"
};

$.ajax(clientQuerySettings).done(function(msg) {
  $("#totalClientsServed").text(msg.results.length);
  //alert(msg.results.length);
});

var clientBranchSettings = {
  "async": true,
  "crossDomain": true,
  "url": "/api/v1/clients/stats/branches",
  "method": "GET"
}

$.ajax(clientBranchSettings).done(function(msg) {
  var total = 0;
  var labels = [];
  for(var i=0;i<msg.buckets.length;i++) {
    total += msg.buckets[i].doc_count;
    labels.push(msg.buckets[i].key);
  }


  var pctData = [];
  var colors = ["blue","green","purple","aero"];
  var table = '<table class="tile_info">';
  for(var i=0;i<msg.buckets.length;i++) {
    pctData.push(Math.round((msg.buckets[i].doc_count / total) * 100));
    table += '<tr><td><p><i class="fa fa-square ' + colors[i] +'"></i>' + msg.buckets[i].key + '</p></td><td>' + Math.round((msg.buckets[i].doc_count / total) * 100) + '%</td></tr><tr><td></tr>';
  }
  table += '</table>';  

  $("#branchDemographics").append(table);

  var options = {
    legend: false,
    responsive: false
  };

  new Chart(document.getElementById("canvas1"), {
    type: 'doughnut',
    tooltipFillColor: "rgba(51, 51, 51, 0.55)",
    data: {
      labels: labels,
      datasets: [{
        data: pctData,
        backgroundColor: [
          "#3d80ed",
          "#26B99A",
          "#9B59B6",
          "#E74C3C"
        ],
        hoverBackgroundColor: [
          "#CFD4D8",
          "#B370CF",
          "#E95E4F",
          "#36CAAB"
        ]
      }]
    },
    options: options
  });

});

var clientGenderSettings = {
  "async": true,
  "crossDomain": true,
  "url": "/api/v1/clients/stats/gender",
  "method": "GET"
}

$.ajax(clientGenderSettings).done(function(msg) {
  for(var i=0;i<msg.buckets.length;i++) {
    var key = msg.buckets[i].key;
    $("#gender_" + key).text(msg.buckets[i].doc_count);
  }
});