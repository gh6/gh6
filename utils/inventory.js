var request = require('request');
var elasticsearch = require('elasticsearch');
var uuid = require('uuid');

var clients = require('./clients');

var awsConfig = require('../config.json');

var http = require('http');

var esClient = new elasticsearch.Client({
  host: "search-gh6-4dgvy3nqmhwzb4nsuird7lxldy.us-east-1.es.amazonaws.com",
  connectionClass: require('http-aws-es'),
  amazonES: {
    region: "us-east-1",
    credentials: awsConfig
  }
});

var householdTypes = ["Households_without_children","Households_with_at_least_one_adult_and_one_child","Households_with_only_children"];
var bedTypes = ["Facility-based","Voucher","Other"];
var youthAgeGroups = ["Under18","18-24","Under24"]

var inventoryFile = {
  "InformationDate": "string",
  "YouthAgeGroup": "string",
  "ShelterName": "Covenant House- Crisis Shelter",
  "HMISParticipatingBeds": "string",
  "ExportID": "string",
  "InventoryStartDate": "string",
  "DateDeleted": "string",
  "BedInventory": "45",
  "VetBedInventory": "string",
  "CoCCode": "string",
  "HouseholdType": "string",
  "DateCreated": "2016-10-22T19:26:56.337Z",
  "DateUpdated": "2016-10-22T19:26:56.337Z",
  "YouthBedInventory": "string",
  "UnitInventory": "35",
  "BedType": "string",
  "CHBedInventory": "string",
  "InventoryEndDate": "string",
  "Availability": "7:30AM-8PM",
  "InventoryID": "string",
  "UserID": "string",
  "ProjectID": "string"
};

var shelters = [
  " Every Child's Hope- TLP",
  " Every Child's Hope-ILP",
  "Almost Home",
  "Annie Malone- ILP",
  "Annie Malone- TLP",
  "Chestnut Health Systems",
  "Convenant House Transitional Housing Program",
  "Covenant House- Crisis Shelter",
  "Criminal Justice Ministry- Release to Rent Veterans",
  "Employment Connections- Project Homecoming- CITY",
  "Epworth- ILP",
  "Epworth- TLP",
  "Epworth- Youth emergency Shelter",
  "Good Shepard- TLP",
  "HUD VASH (project based- 16 Salvation Army vs. scatter site)",
  "Joseph's Center",
  "Karen House",
  "Marygrove- Emergency Shelter",
  "Marygrove- Independent Living",
  "Marygrove- transitional housing",
  "Missionaries of Charity",
  "Missouri Baptist",
  "Missouri Baptist Group Home- TLP",
  "NLEC- men's shelter",
  "NLEC- women/families",
  "Opal's House",
  "SSVF- EC (subcontract of SPC funds)",
  "SSVF- SPC",
  "Saint Patrick Center (GPD- Project HERO)",
  "Saint Patrick Center- Veterans Housing Resource (VHR Project HERO)",
  "Saint Vincent- ES",
  "Salvation Army (GPD)",
  "Shelter Plus Care Vouchers",
  "Sunshine Ministries",
  "The Haven of Grace- TLP",
  "The Haven of Grace- crisis shelter",
  "Veterans Administation- VADOM-VA Domicillary",
  "Youth in Need- Crisis Shelter",
  "Youth in Need- ILP",
  "Youth in Need- TLP",
  "Annie Malone- Crisis Shelter"];



function randomDate(start, end) {
  return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
}

function createRandomData(numData) {

  _getAllUserUuids(function(uuids){

    //for(var i=0;i<numData;i++) {
    var now = new Date();
    for (var d = new Date(2016, 6, 1); d <= now; d.setDate(d.getDate() + 1)) {
      for(var i=0;i<shelters.length;i++) {

        var inv = inventoryFile;

        var randUuidIndex = Math.floor(Math.random() * uuids.length - 1) + 1;
        inv.UserID = uuids[randUuidIndex];

        //var rd = randomDate(new Date(2016, 0, 1), new Date()).toISOString();
        inv.DateCreated = d.toISOString();
        inv.DateUpdated = d.toISOString();

        var randShlterIndex = Math.floor(Math.random() * shelters.length - 1) + 1;
        inv.ShelterName = shelters[i];

        var randBedInventor = Math.floor(Math.random() * 50) + 1;
        inv.BedInventory = randBedInventor + "";

        var youthBedInventory = Math.floor(Math.random() * 10) + 1;
        inv.YouthBedInventory = youthBedInventory + "";

        var householdType = Math.floor(Math.random() * householdTypes.length) + 1;
        inv.HouseholdType = householdTypes[householdType];

        var bedType = Math.floor(Math.random() * bedTypes.length) + 1;
        inv.BedType = bedTypes[bedType];

        var youthAgeGroup = Math.floor(Math.random() * youthAgeGroups.length) + 1;
        inv.YouthAgeGroup = youthAgeGroups[youthAgeGroup];

        console.log(inv);

        /*
         esClient.index({
         index: "inventory",
         id: uuid.v4(),
         type: "inventory",
         body: inv
         }, function (err, res) {
         if (err) {
         var payload = {
         error: "Error indexing tweet to ES.",
         trace: err
         };
         console.log("Inventory Error");
         } else {

         console.log("Inventory Added");
         }
         });
         */

        /*
         var options = {
         url: "http://52.45.164.223/api/v1/inventory",
         method: "POST",
         json: true,
         headers: {
         "content-type": "application/json",
         "Authorization": "Basic bmdpbng6Z2g2"
         },
         body: inv
         };

         request(options, function (err, resp, body) {
         if(err) {
         console.log(err);
         } else {
         console.log(body.code);
         }
         });
         */
      }

    }
  });
}

createRandomData(10);




function _getAllUserUuids(callback) {
  var uuids = [];
  var options = {
    host: 'localhost',
    port: '3002',
    path: '/api/v1/clients',
    method: 'GET'
  };

  http.request(options, function(res) {
    res.setEncoding('utf8');
    var body = '';
    res.on('data', function (chunk) {
      body += chunk;
    });
    res.on('end', function(){
      var parsed = JSON.parse(body);
      for (var i = 0; i < parsed.results.length; i++){
        uuids.push(parsed.results[i].uuid);
      }
      callback(uuids);
    });
  }).end();
}

