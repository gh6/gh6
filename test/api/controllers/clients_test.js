'use strict';

var expect      = require('chai').expect;
var clients     = require('../../../api/controllers/clients');

describe("Testing Get Clients", function () {
  it("Create Record From Metadata and Definition", function (done) {
    this.timeout(1000);
    clients.test.getAllClients(function(err,data) {
      expect(err).to.be.null;
      expect(data.count).to.equal(data.results.length);
      done();
    });
  });
});

describe("Testing get a client", function() {
  it("Get Client by UUID", function(done) {
    var testUUID = "247611";
    clients.test.getClientByUUID(testUUID, function(err,data) {

      expect(err).to.be.null;
      expect(data.count).to.equal(1);
      expect(data.uuid).to.equal(testUUID);

      done();
    });
  });
});

describe("Testing create a client", function() {
  it("Create Client", function(done) {
    var client = {
      Name: {
        First: "Test",
        Middle: "J",
        Last: "User"
      },
      SSN: "133090088",
      DOB: "1/1/73",
      Race: "Black",
      Gender: "Male",
      Veteran: {
        "Entered": "1981",
        "Seperated": "1993",
        "WARS": null,
        "Branch" : "Marines",
        "Discharge": "General under honorable conditions"
      },
      UserID: "182894"
    };
    clients.test.createNewClient(client, function(err,data) {

      expect(err).to.be.null;
      expect(data.count).to.equal(1);
      console.log(data);

      done();
    });
  });
});