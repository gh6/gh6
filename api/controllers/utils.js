/**
 * @file
 * Util functions
 *
 */

var httpStatusCodes = require('http-status-codes');
var logger = require('../utils/logger').logger;

module.exports = {
  readCoCCodes: function (req, res) {
    var coCCodes = require('../utils/CoCCodes.json');
    return res.status(httpStatusCodes.OK).json({coCCodes: coCCodes});
  }
};
