var elasticsearch = require('elasticsearch');
var httpStatusCodes    = require('http-status-codes');

var config = require('../../config.json');

var esClient = new elasticsearch.Client({
  host: "search-gh6-4dgvy3nqmhwzb4nsuird7lxldy.us-east-1.es.amazonaws.com",
  connectionClass: require('http-aws-es'),
  amazonES: {
    region: "us-east-1",
    credentials: config
  }
});

var branchStats = {
  "query": {
    "filtered": {
      "query": {
        "query_string": {
          "query": "*",
          "analyze_wildcard": true
        }
      },
      "filter": {
        "bool": {
          "must": [
            {
              "range": {
                "created": {
                  "gte": 1319281004595,
                  "lte": 1477133804595,
                  "format": "epoch_millis"
                }
              }
            }
          ],
          "must_not": []
        }
      }
    }
  },
  "size": 0,
  "aggs": {
    "branches": {
      "terms": {
        "field": "veteran.branch",
        "size": 5,
        "order": {
          "_count": "desc"
        }
      }
    }
  }
};

function getBranchStats(callback) {
  esClient.search({
    index: 'client',
    body: branchStats
  }, function elasticSearchCallback(err, response) {
    if (err) {
      var errorPayload = {
        status: "error",
        statusCode: httpStatusCodes.INTERNAL_SERVER_ERROR,
        error: "Failed Retrieving Branches Histogram",
        timestamp: new Date().toJSON(),
        trace: err
      };
      callback(errorPayload);
    } else {
      var payload = {
        status:"success",
        statusCode: httpStatusCodes.OK,
        "buckets": response.aggregations.branches.buckets,
        timestamp: new Date().toJSON()
      };
      callback(null, payload);
    }
  });
}

var queryGender = {
  "size": 0,
  "query": {
    "filtered": {
      "query": {
        "query_string": {
          "analyze_wildcard": true,
          "query": "*"
        }
      },
      "filter": {
        "bool": {
          "must": [
            {
              "range": {
                "createdInEs": {
                  "gte": 1003779528360,
                  "lte": 1477165128360,
                  "format": "epoch_millis"
                }
              }
            }
          ],
          "must_not": []
        }
      }
    }
  },
  "aggs": {
    "genders": {
      "terms": {
        "field": "gender",
        "size": 20,
        "order": {
          "_count": "desc"
        }
      }
    }
  }
};

function getGenderStats(callback) {
  esClient.search({
    index: 'client',
    body: queryGender
  }, function elasticSearchCallback(err, response) {
    if (err) {
      var errorPayload = {
        status: "error",
        statusCode: httpStatusCodes.INTERNAL_SERVER_ERROR,
        error: "Failed Retrieving Branches Histogram",
        timestamp: new Date().toJSON(),
        trace: err
      };
      callback(errorPayload);
    } else {
      var payload = {
        status:"success",
        statusCode: httpStatusCodes.OK,
        "buckets": response.aggregations.genders.buckets,
        timestamp: new Date().toJSON()
      };
      callback(null, payload);
    }
  });
}

module.exports = {
  getBranchStats:function(req,res) {
    getBranchStats(function(err,data) {
      if(err) {
        res.status(err.statusCode).json(err);
      } else {
        res.status(data.statusCode).json(data);
      }
    });
  },
  getGenderStats:function(req,res) {
    getGenderStats(function(err,data) {
      if(err) {
        res.status(err.statusCode).json(err);
      } else {
        res.status(data.statusCode).json(data);
      }
    });
  },
  test: {
    getBranchStats:getBranchStats,
    getGenderStats:getGenderStats
  }
};