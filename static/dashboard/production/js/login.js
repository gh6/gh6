var settings = {
  "async": true,
  "crossDomain": true,
  "url": "/solr2/gh6/select?facet.field=Program_Name&facet=on&indent=on&q=*:*&wt=json",
  "method": "GET"
};

$.ajax(settings).done(function (response) {


  var pResponse = JSON.parse(response);

  var pNames = pResponse.facet_counts.facet_fields.Program_Name;

  var formElem = "";//'<label id="shelter_label" for="shelter_type">Select a Shelter Type to get Started *:</label>';
  formElem += '<select id="shelters" name="shelter" class="form-control" required onchange="populateFields(this)">';
  formElem += '<option value="">Select your Shelter...</option>';
  for(var i=0;i<pNames.length;i+=2) {
    console.log(pNames[i]);
    formElem += '<option value="' + pNames[i] + '">' + pNames[i] + '</option>';
  }
  formElem += '</select>';
  $("#shelter_combo").append(formElem);

});