var winston = require('winston');
//var os = require('os');

//require('winston-logstash');
//require('winston-logstash-udp');

var logger = new winston.Logger();

function setupLogger() {
  /*
  logger.add(winston.transports.Logstash, {
    port: 28777,
    node_name: os.hostname(),
    host: '10.0.0.159'
  });

  logger.add(winston.transports.File, {
    filename: 'ccapi.log'
  });*/
  logger.add(winston.transports.Console);

  logger.info("Logging Setup");

}

setupLogger();

module.exports = {
  logger : logger
};