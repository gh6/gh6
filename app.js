'use strict';

var SwaggerExpress = require('swagger-express-mw');
var express = require('express');
var app = express();
var compression = require('compression');
var cors = require('cors');
var logger = require('./api/utils/logger').logger;
var Http = require('http');

module.exports = app;
// Enable CORS pre-flight
app.options('*', cors());

app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET, POST, DELETE, OPTIONS, PUT, PATCH');
  res.header('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
  res.header('Access-Control-Allow-Headers', 'Content-Type');
  next();
});

var config = {
  appRoot: __dirname
};
SwaggerExpress.create(config, function (err, swaggerExpress) {
  if (err) {
    throw err;
  }

  app.use('/docs', express.static(__dirname + '/node_modules/swagger-ui/dist'));
  app.use('/', express.static(__dirname + "/static/dashboard/production"));
  app.use('/vendors', express.static(__dirname + "/static/dashboard/vendors"));
  app.use('/build', express.static(__dirname + "/static/dashboard/build"));
  app.use('/cards', express.static(__dirname + "/static/dashboard/production/cards.html"));

  app.use('/solr2', getSolr);


  // Enable compression on all responses
  app.use(compression());

  // install middleware
  swaggerExpress.register(app);

  app.use(function (err, req, res, next) {
    res.status(500).send(err);
    next();
  });

  var port = process.env.PORT || 3002;
  app.listen(process.env.PORT || port);

  logger.info({
    "method": "appStart",
    "message": 'Dev API Server running on http://127.0.0.1:' + port
  });
  console.log('Dev API Server running on http://127.0.0.1:' + port);
});

function getSolr(req,res,next) {
  var request = require("request");

  var options = { method: 'GET',
    url: 'http://52.45.164.223/solr/' + req.url,
    headers:
    { 'cache-control': 'no-cache',
      authorization: 'Basic bmdpbng6Z2g2' } };

  request(options, function (error, response, body) {
    if (error) throw new Error(error);
    res.end(body);
    //next();
    //console.log(body);
  });
}