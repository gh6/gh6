$(document).ready(function() {
  startKiosk();
})

function startKiosk(){

  $.typeahead({
    input: ".js-typeahead-lastname",
    order: "asc",
    minLength: 1,
    maxItem: 20,
    dynamic: true,
    offset: true,
    debug: true,
    hint: true,
    display: ["name.last", "name.first"],
    template: '<span id={{uuid}}>{{name.last}}, {{name.first}}</span>',
    source: {
      lastName: {
        ajax: function(query) {
          query = '{"LastName": "' + query + '"}';
          return {
            type: "POST",
            url: "/api/v1/clients/search",
            path: "results",
            dataType: "json",
            contentType: "application/json",
            data: query
          }
        }
      }
    },
    callback: {
      onClickAfter: function (node, a, item, event) {
        $('#firstName').val(item.name.first);
        $('#uuid').val(item.uuid);
        $('.autoFilledData').show()
        event.preventDefault;
      }
    }
  });



  $.typeahead({
    input: ".js-typeahead-ssn",
    order: "asc",
    minLength: 4,
    maxItem: 20,
    dynamic: true,
    offset: true,
    debug: true,
    hint: true,
    source: {
      ssn: {
        ajax: function(query) {
          query = '{"ssn": "' + query + '", "uuid": "'+ $("#uuid").val() +'"}';
          return {
            type: "POST",
            url: "/api/v1/clients/verifySSN",
            path: "result",
            dataType: "json",
            contentType: "application/json",
            data: query
          }
        }
      }
    },
    callback: {
      onPopulateSource: function (node, a, item, event) {
        event.preventDefault;
        if (a.length !== 0) {
          $('input#ssn').attr("disabled", true);
          $('input#lastName').attr("disabled", true);
          $('#formAlert').removeClass("alert-info").removeClass("alert-danger").addClass("alert-success").html("Thank you. SSN has been verified.");
          a[0].ssn4 = $('input#ssn').val();
          setTimeout(function(){ showInitalForm(a[0], 'welcomeForm', 'initialForm')}, 1500);
        } else {
          $('#formAlert').removeClass("alert-info").addClass("alert-danger").html("Sorry. Incorrect SSN. Please try again.");
        }
      }
    }
  });



}

function showInitalForm(user, from, to){
  $('#registerNew').hide();
  $('#'+from).slideUp(300);
  if (typeof(user) != "undefined") { populateUserFieldsInitialForm(user); }
  var initForm = $('#' + to +' form').detach();
  $('.x_content').empty();
  initForm.appendTo($('.x_content'));
  $('#'+to).slideDown(300);
}

function populateUserFieldsInitialForm(user) {
  var form = $('#initialForm form');
  form.find('#firstname').val(user.name.first);
  form.find('#middleinitial').val(user.name.middle);
  form.find('#lastname').val(user.name.last);
  form.find('#dob').val(moment(user.dob).format('YYYY-MM-DD'));
  form.find('#socialnum').val("###-##-"+user.ssn4);
  form.find('select option[value="'+ user.gender.toLowerCase().trim().replace(/'/, '') +'"]').attr("selected", true);
  form.find('select option[value="'+ user.race.toLowerCase().trim().replace(/'/, '') +'"]').attr("selected", true);
  var vetform = $('#veteranForm form');
  if (typeof(user.veteran) != undefined) {
    //vetform.find('#veteranbranch').val( (typeof(user.veteran.branch) != undefined) ? user.veteran.branch.toLowerCase().trim().replace(/'/, '') +'"]').attr("selected", true) : "" );
    vetform.find('#veteranentered').val( (typeof(user.veteran.entered) != undefined) ? user.veteran.entered : "" );
    vetform.find('select option[value="'+ user.veteran.discharge.toLowerCase().trim().replace(/'/, '') +'"]').attr("selected", true);
    if (typeof(user.veteran.wars) != undefined) {
      for (var i = 0; i < user.veteran.wars.length; i++) {
        vetform.find('select option[value="'+ user.veteran.wars[i].toLowerCase().trim().replace(/'/, '') +'"]').attr("checked", true);
      }
    }
  }



}