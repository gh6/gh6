var settings = {
  "async": true,
  "crossDomain": true,
  "url": "/solr2/gh6/select?indent=on&q=*:*&rows=50&wt=json",
  "method": "GET"
};

$.ajax(settings).done(function (response) {
  var headers = ["Program_Name","Type","Must_have_ID","Service_Capacity_Total","Do_They_Serve_Families","Address"];
  var tHead = "<tr>";
  for(var i=0;i<headers.length;i++) {
    tHead += "<th>" + headers[i].replace(/_/g," ") + "</th>";
  }
  tHead += "</tr>";

  $("#datatableThead").append(tHead);

  var data = JSON.parse(response).response.docs;
  for(var i=0;i<data.length;i++) {
    var tBody = "<tr>";
    for(var j=0;j<headers.length;j++) {
      if(headers[j] === "Address") {
        var dVal = (data[i][headers[j]] === undefined) ? ' ' : data[i][headers[j]];
        tBody += "<td><a target='_new' href='https://www.google.com/maps?espv=2&q=" + dVal + "'>" + dVal + "</a></td>";
      } else if(headers[j] === "Program_Name") {
        var dVal = (data[i][headers[j]] === undefined) ? ' ' : data[i][headers[j]];
        tBody += "<td><a href='shelter_update.html?id=" + data[i]["id"] + "'>" + dVal + "</a></td>";
      } else {
        var dVal = (data[i][headers[j]] === undefined) ? ' ' : data[i][headers[j]];
        tBody += "<td>" + dVal + "</td>";
      }

    }
    tBody += "</tr>";
    $("#datatableTbody").append(tBody);
  }
  
  var handleDataTableButtons = function() {
          if ($("#datatable-buttons").length) {
            $("#datatable-buttons").DataTable({
              dom: "Bfrtip",
              buttons: [
                {
                  extend: "copy",
                  className: "btn-sm"
                },
                {
                  extend: "csv",
                  className: "btn-sm"
                },
                {
                  extend: "excel",
                  className: "btn-sm"
                },
                {
                  extend: "pdfHtml5",
                  className: "btn-sm"
                },
                {
                  extend: "print",
                  className: "btn-sm"
                },
              ],
              responsive: true
            });
          }
        };

        TableManageButtons = function() {
          "use strict";
          return {
            init: function() {
              handleDataTableButtons();
            }
          };
        }();

        $('#datatable').dataTable();

        

        TableManageButtons.init();


});