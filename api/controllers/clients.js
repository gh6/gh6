'use strict';

var AWS = require("aws-sdk");
var httpStatusCodes    = require('http-status-codes');
var uuid               = require('uuid');

AWS.config.loadFromPath('./config.json');
AWS.config.update({region: 'us-east-1'});

var dynamodbDoc = new AWS.DynamoDB.DocumentClient();

var clientTableName = "clients";

function getAllClients(callback) {
  var params = {
    TableName: clientTableName
  };

  dynamodbDoc.scan(params, function(err,data) {
    if(err) {
      var payload = {
        status: "error",
        statusCode: httpStatusCodes.INTERNAL_SERVER_ERROR,
        error:      "Failed Retrieving Clients",
        trace: err
      };
      callback(payload,null);
    } else {
      var payload = {
        count: data.Count,
        status: "success",
        statusCode: httpStatusCodes.OK,
        message: "Clients Received",
        results: data.Items
      };
      callback(null,payload);
    }
  });
}

function getClientByUUID(uuid,callback) {
  var params = {
    TableName: clientTableName,
    Key:       {
      uuid: uuid
    }
  };

  dynamodbDoc.get(params,function(err,data) {
    if(err) {
      var payload = {
        status:     "error",
        statusCode: httpStatusCodes.INTERNAL_SERVER_ERROR,
        error:      "Failed Reading Client",
        timestamp:  new Date().toJSON()
      };
    } else {
      var count   = data.Item ? 1 : 0;
      var result  = data.Item ? data.Item : {};
      var payload = {
        count:      count,
        status:     "success",
        statusCode: httpStatusCodes.OK,
        message:    "Client Read",
        uuid:       uuid,
        result:     result,
        timestamp:  new Date().toJSON()
      };
      callback(null, payload);
    }
  });
}

function searchClientsByLastName(lastName, callback) {
  getAllClients(function(x, data){
    var newResults = [];
    for (var i = 0; i < data.results.length; i++) {
      try {
        if (typeof(data.results[i].name) != "undefined") {
          if (typeof(data.results[i].name.last) != "undefined") {
            if (data.results[i].name.last.substring(0, lastName.length).toLowerCase() === lastName.toLowerCase()) {
              delete data.results[i].ssn;
              newResults.push(data.results[i]);
            }
          }
        }
        if (typeof(data.results[i].Name) != "undefined") {
          if (typeof(data.results[i].Name.Last) != "undefined") {
            if (data.results[i].Name.Last.substring(0, lastName.length).toLowerCase() === lastName.toLowerCase()) {
              delete data.results[i].ssn;
              newResults.push(data.results[i]);
            }
          }
        }
      } catch (ex) {
      }
    };
    if (newResults.length === 0) {
      data.count = 0;
      data.results = [];
    } else {
      data.count = newResults.length;
      data.results = newResults;
    }
    callback(null, data);
  });
/*  var params = {
    TableName: clientTableName,
    FilterExpression:
      "begins_with(#lname, :lname)",
    ExpressionAttributeNames: {
      "#lname": "name.last"
    },
    ExpressionAttributeValues: {
      ':lname': lastName
    }
  };

  dynamodbDoc.scan(params,function(err,data) {
    if(err) {
      var payload = {
        status:     "error",
        statusCode: httpStatusCodes.INTERNAL_SERVER_ERROR,
        error:      "Failed Reading Client",
        timestamp:  new Date().toJSON()
      };
    } else {
      var count   = data.Item ? 1 : 0;
      var result  = data.Item ? data.Item : {};
      var payload = {
        count:      count,
        status:     "success",
        statusCode: httpStatusCodes.OK,
        message:    "Client Read",
        uuid:       uuid,
        result:     result,
        timestamp:  new Date().toJSON()
      };
      callback(null, payload);
    }
  });*/
}

function verifySSN(body, callback) {
  getClientByUUID(body.uuid, function(x, data){
    if (data.status === "error") {
      callback(null, data);
    } else {
      if (data.result.ssn.substring(data.result.ssn.length - 4) === body.ssn) {
        delete data.result.ssn;
        var tmp = data.result;
        delete data.result;
        data.result = [];
        data.result.push(tmp);
        data.message = "SSN Verified";
        callback(null, data);
      } else {
        delete data.result;
        data.result = [];
        data.message = "SSN Failed Verification";
        callback(null, data);
      }
    }
  });
}


function createNewClient(client,callback) {
  var curDate = new Date().toJSON();
  client.uuid = uuid.v1();

  client.created = curDate;
  client.updated = curDate;

  var params = {
    TableName: clientTableName,
    Item:client,
    ExpressionAttributeNames: {"#u": "uuid"},
    ConditionExpression:      "attribute_not_exists(#u)"
  };

  dynamodbDoc.put(params, function (err, data) {
    if (err) {
      var payload = {
        status:     "error",
        statusCode: httpStatusCodes.INTERNAL_SERVER_ERROR,
        error:      "Failed Creating Client",
        timestamp:  new Date().toJSON()
      };
      callback(err, payload);
    }
    else {
      var payload = {
        count:      1,
        status:     "success",
        statusCode: httpStatusCodes.OK,
        message:    "Client Created",
        results:    params.Item,
        timestamp:  new Date().toJSON()
      };
      callback(null, payload);
    }
  });
}

module.exports = {
  getAllClients: function(req,res) {
    getAllClients(function(err,data) {
      if(err) {
        res.status(err.statusCode).json(err);
      } else {
        res.status(data.statusCode).json(data);
      }
    });
  },
  getClientByUUID: function(req,res) {
    var uuid = req.swagger.params.uuid.value;
    getClientByUUID(uuid,function(err,data) {
      if(err) {
        res.status(err.statusCode).json(err);
      } else {
        res.status(data.statusCode).json(data);
      }
    });
  },
  searchClientsByLastName: function(req,res) {
    var lastName = req.swagger.params.body.value.LastName;
    searchClientsByLastName(lastName,function(err,data) {
      if(err) {
        res.status(err.statusCode).json(err);
      } else {
        res.status(data.statusCode).json(data);
      }
    });
  },
  createNewClient: function(req,res) {
    var client = req.swagger.params.body.value;
    createNewClient(client, function(err,data) {
      if(err) {
        res.status(err.statusCode).json(err);
      } else {
        res.status(data.statusCode).json(data);
      }
    });
  },
  verifySSN: function(req,res) {
    var client = req.swagger.params.body.value;
    verifySSN(client, function (err, data) {
      if (err) {
        res.status(err.statusCode).json(err);
      } else {
        res.status(data.statusCode).json(data);
      }
    });
  },
  test: {
    getAllClients:getAllClients,
    getClientByUUID:getClientByUUID,
    createNewClient:createNewClient,
    searchClientsByLastName:searchClientsByLastName,
    verifySSN:verifySSN
  }
};