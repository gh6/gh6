var settings = {
  "async": true,
  "crossDomain": true,
  "url": "/api/v1/inventory",
  "method": "GET"
};

$.ajax(settings).done(function (response) {
  var headers = ["ShelterName","Availability","UnitInventory","BedInventory"];
  var tHead = "<tr>";
  for(var i=0;i<headers.length;i++) {
    tHead += "<th>" + headers[i] + "</th>";
  }
  tHead += "</tr>";

  $("#datatableThead").append(tHead);

  var data = response.results;

  for(var i=0;i<data.length;i++) {
    var tBody = "<tr>";
    for(var j=0;j<headers.length;j++) {
      var dVal = (data[i][headers[j]] === undefined) ? ' ' : data[i][headers[j]];
      tBody += "<td>" + dVal + "</td>";
    }
    tBody += "</tr>";
    $("#datatableTbody").append(tBody);
  }
  
  var handleDataTableButtons = function() {
          if ($("#datatable-buttons").length) {
            $("#datatable-buttons").DataTable({
              dom: "Bfrtip",
              buttons: [
                {
                  extend: "copy",
                  className: "btn-sm"
                },
                {
                  extend: "csv",
                  className: "btn-sm"
                },
                {
                  extend: "excel",
                  className: "btn-sm"
                },
                {
                  extend: "pdfHtml5",
                  className: "btn-sm"
                },
                {
                  extend: "print",
                  className: "btn-sm"
                },
              ],
              responsive: true
            });
          }
        };

        TableManageButtons = function() {
          "use strict";
          return {
            init: function() {
              handleDataTableButtons();
            }
          };
        }();

        $('#datatable').dataTable();

        

        TableManageButtons.init();


});